/*
 * Copyright (c) 2020 com.company.task.entity
 */
package com.company.task.entity;


/**
 * @author serge
 */
import com.haulmont.cuba.core.entity.annotation.EnableRestore;
import com.haulmont.cuba.core.entity.annotation.TrackEditScreenHistory;
import javax.persistence.Entity;
import javax.persistence.DiscriminatorValue;
import javax.persistence.InheritanceType;
import javax.persistence.Inheritance;
import com.haulmont.cuba.core.entity.annotation.Listeners;

@Listeners("task_AutoListener")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("2")
@Entity(name = "task$Auto")
@EnableRestore
@TrackEditScreenHistory
public class Auto extends AutoBase {
    private static final long serialVersionUID = -7236061743458682891L;

}