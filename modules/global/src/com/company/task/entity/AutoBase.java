/*
 * Copyright (c) 2020 com.company.task.entity
 */
package com.company.task.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.DiscriminatorValue;
import javax.persistence.InheritanceType;
import javax.persistence.Inheritance;
import javax.persistence.PrimaryKeyJoinColumn;
import com.haulmont.cuba.core.entity.annotation.SystemLevel;
import com.haulmont.thesis.core.annotation.CardBase;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.haulmont.thesis.core.entity.TsCard;

/**
 * @author serge
 */
@CardBase
@SystemLevel
@PrimaryKeyJoinColumn(name = "CARD_ID", referencedColumnName = "ID")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("1")
@Table(name = "TASK_AUTO_BASE")
@Entity(name = "task$AutoBase")
public class AutoBase extends TsCard {
    private static final long serialVersionUID = 4986276849907987214L;

    @Column(name = "NUMBER_", length = 50)
    protected String number;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BRAND_ID")
    protected Brand brand;

    @Column(name = "NAME", length = 50)
    protected String name;

    @Column(name = "YEAR_", length = 50)
    protected Integer year;

    @Column(name = "COST", length = 50)
    protected Integer cost;

    @Column(name = "TYPE_AUTO", length = 50)
    protected Integer typeAuto;

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getYear() {
        return year;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Integer getCost() {
        return cost;
    }

    public void setTypeAuto(Integer typeAuto) {
        this.typeAuto = typeAuto;
    }

    public Integer getTypeAuto() {
        return typeAuto;
    }


}