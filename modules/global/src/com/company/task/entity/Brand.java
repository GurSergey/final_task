/*
 * Copyright (c) 2020 com.company.task.entity
 */
package com.company.task.entity;


/**
 * @author serge
 */
import com.haulmont.cuba.core.entity.annotation.EnableRestore;
import com.haulmont.cuba.core.entity.annotation.TrackEditScreenHistory;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.haulmont.chile.core.annotations.NamePattern;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;

@NamePattern("%s|name")
@Table(name = "TASK_BRAND")
@Entity(name = "task$Brand")
@EnableRestore
@TrackEditScreenHistory
public class Brand extends StandardEntity {
    private static final long serialVersionUID = -498433123405790674L;

    @Column(name = "NAME", length = 50)
    protected String name;

    @Column(name = "CODE", length = 50)
    protected String code;

    @Column(name = "DESCR", length = 50)
    protected String descr;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getDescr() {
        return descr;
    }


}