/*
 * Copyright (c) 2020 com.company.task.entity
 */
package com.company.task.entity;

import com.haulmont.chile.core.datatypes.impl.EnumClass;

import javax.annotation.Nullable;

/**
 * @author serge
 */
public enum TypeAuto implements EnumClass<Integer> {

    Crossover(1),
    Wagon(2),
    Sedan(3);

    private Integer id;

    TypeAuto(Integer value) {
        this.id = value;
    }

    public Integer getId() {
        return id;
    }

    @Nullable
    public static TypeAuto fromId(Integer id) {
        for (TypeAuto at : TypeAuto.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}