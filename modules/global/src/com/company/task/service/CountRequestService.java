/*
 * Copyright (c) 2020 com.company.task.service
 */
package com.company.task.service;

import com.haulmont.thesis.core.entity.Contractor;

/**
 * @author serge
 */
public interface CountRequestService {
    String NAME = "task_CountRequestService";
    public Long getCountForCustomer(Contractor customer);
}