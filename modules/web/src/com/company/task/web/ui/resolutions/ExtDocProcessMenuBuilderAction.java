/*
 * Copyright (c) ${YEAR} ${PACKAGE_NAME}
 */

package com.company.task.web.ui.resolutions;

import com.company.task.entity.PurchaseRequest;
import com.haulmont.cuba.core.global.UserSessionSource;
import com.haulmont.cuba.gui.components.Action;
import com.haulmont.cuba.security.entity.Role;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.thesis.web.ui.common.processmenu.ProcessedCardsHolder;
import com.haulmont.thesis.web.ui.simpledoc.DocProcessMenuBuilderAction;
import com.haulmont.workflow.core.entity.Card;
import com.haulmont.workflow.core.entity.CardRole;

import javax.inject.Inject;
import javax.print.Doc;
import java.util.List;

public class ExtDocProcessMenuBuilderAction extends DocProcessMenuBuilderAction {

    @Inject
    protected UserSessionSource userSessionSource;

    protected static final String NAME_ROLE = "Operator";
    protected static final String NAME_PROC = "Assignment.checkRequest";

    @Override
    protected void internalBuildActions(ProcessedCardsHolder processedCardsHolder) {
        super.internalBuildActions(processedCardsHolder);

        Card card = cards.iterator().next();

        if(card instanceof PurchaseRequest) {
            if(!((PurchaseRequest) card).getPaid() &&
                    isCurrentUserExecutor(card.getRoles())){
                for(Action a: actions) {
                    if(a.getId().equals(NAME_PROC))
                    {
                        a.setEnabled(false);
                    }
                }
            }
        }
    }

    protected boolean isCurrentUserExecutor(List<CardRole> roleList){
        User user = userSessionSource.getUserSession().getCurrentOrSubstitutedUser();
        for (CardRole role: roleList) {
            if(user.equals(role.getUser()))
                return role.getProcRole().getCode().equals(NAME_ROLE);
        }
        return false;
    }
}
