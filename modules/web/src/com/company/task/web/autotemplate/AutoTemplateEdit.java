
package com.company.task.web.autotemplate;

import com.company.task.entity.AutoTemplate;
import com.company.task.web.auto.AutoEdit;

public class AutoTemplateEdit extends AutoEdit<AutoTemplate> {

    @Override
    protected boolean isNumberAssignNeeded() {
        return false;
    }

    @Override
    protected boolean isImportantButtonVisible() {
        return false;
    }
}