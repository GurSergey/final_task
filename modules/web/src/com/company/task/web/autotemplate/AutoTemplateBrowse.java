
package com.company.task.web.autotemplate;

import com.company.task.entity.Auto;
import com.company.task.entity.AutoBase;
import com.company.task.entity.AutoTemplate;
import com.company.task.web.auto.AutoBrowse;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.thesis.web.ui.common.actions.CreateCardFromTemplateAction;

import javax.inject.Inject;
import java.util.Map;

public class AutoTemplateBrowse extends AutoBrowse<AutoTemplate> {

    @Inject
    protected Button createCardFromTemplateButton;
    @Inject
    protected Metadata metadata;

    @Override
    public void init(Map<String, Object> params) {
        super.init(params);
        initCreateCardFromTemplateAction();
    }

    protected void initCreateCardFromTemplateAction() {
        createCardFromTemplateButton.setAction(new CreateCardFromTemplateAction<AutoBase>(
                metadata.getClass(Auto.class), this, "createCardFromTemplate"));
    }

    protected boolean isCreateByTemplateActionEnabled() {
        return false;
    }

    @Override
    protected void addImportantColumn() {
    }

    @Override
    protected void addLocStateColumn() {
    }

    @Override
    protected void initCardDetailsFunctionality() {
    }

    @Override
    protected boolean isFilterImportantConditionEnabled() {
        return false;
    }
}