/*
 * Copyright (c) 2020 com.company.task.web.individualbrowse
 */
package com.company.task.web.individualbrowse;

import com.company.task.service.CountRequestService;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.components.Label;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import com.haulmont.thesis.core.entity.Individual;
import com.haulmont.thesis.web.ui.individual.IndividualBrowser;

import javax.inject.Inject;
import java.util.Map;

/**
 * @author serge
 */
public class ExtIndividualBrowser extends IndividualBrowser {
    @Inject
    CountRequestService service;

    @Inject
    private GroupTable individualsTable;

    @Inject
    private ComponentsFactory componentsFactory;

    @Override
    public void init(Map<String, Object> params) {
        individualsTable.addGeneratedColumn("numbAuto", new Table.ColumnGenerator<Individual>() {
            @Override
            public Component generateCell(Individual entity) {
                Label field = componentsFactory.createComponent(Label.class);
                field.setValue(service.getCountForCustomer(entity));
                return field;
            }
        });
        individualsTable.addPrintable("numbAuto", new Table.Printable<Individual, String>() {
            @Override
            public String getValue(Individual entity) {
                return String.valueOf(service.getCountForCustomer(entity));
            }
        });
    }
}
