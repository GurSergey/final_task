/*
 * Copyright (c) 2020 com.company.task.web.companybrowse
 */
package com.company.task.web.companybrowse;

import com.company.task.service.CountRequestService;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.GroupTable;
import com.haulmont.cuba.gui.components.Label;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import com.haulmont.thesis.core.entity.Company;
import com.haulmont.thesis.core.entity.Individual;
import com.haulmont.thesis.web.ui.company.CompanyBrowser;


import javax.inject.Inject;
import java.util.Map;

/**
 * @author serge
 */
public class ExtCompanyBrowser extends CompanyBrowser {
    @Inject
    CountRequestService service;
    @Inject
    private GroupTable companyTable;

    @Inject
    private ComponentsFactory componentsFactory;

    @Override
    public void init(Map<String, Object> params) {
        companyTable.addGeneratedColumn("numbAuto", new Table.ColumnGenerator<Company>() {
            @Override
            public Component generateCell(Company entity) {
                Label field = componentsFactory.createComponent(Label.class);
                field.setValue(service.getCountForCustomer(entity));
                return field;
            }
        });
        companyTable.addPrintable("numbAuto", new Table.Printable<Company, String>() {
            @Override
            public String getValue(Company entity) {
                return String.valueOf(service.getCountForCustomer(entity));
            }
        });
    }
}
