--Add default doc kind for task$PurchaseRequest
CREATE OR REPLACE FUNCTION baseInsert()
RETURNS integer
AS $$
DECLARE
cnt integer = 0;
BEGIN
cnt = (select count(CATEGORY_ID) from DF_DOC_KIND where category_id = '96cd5171-b893-4743-8e86-60e52ee542d1');
if(cnt = 0) then
    insert into SYS_CATEGORY (ID, NAME, ENTITY_TYPE, IS_DEFAULT, CREATE_TS, CREATED_BY, VERSION, DISCRIMINATOR)
    values ( '96cd5171-b893-4743-8e86-60e52ee542d1', 'Заявка на покупку автомобиля', 'task$PurchaseRequest', false, now(), USER, 1, 1);

    insert into DF_DOC_KIND (category_id, create_ts, created_by, version, doc_type_id, numerator_id, 
    numerator_type, category_attrs_place, tab_name, portal_publish_allowed, disable_add_process_actors, create_only_by_template)
    values ('96cd5171-b893-4743-8e86-60e52ee542d1', 'now()', 'admin', 1, '163da838-b3d0-4c8e-bd4d-1158e2379d63', 'fc017a83-7570-4ca6-853a-156430daffed', 
    1, 1, 'Р”РѕРї. РїРѕР»СЏ', false, false, false);
end if;return 0;

END;
$$
LANGUAGE plpgsql;
^
select baseInsert();
^
drop function if exists baseInsert()^
