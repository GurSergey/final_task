-- alter table
INSERT INTO sec_role (ID, CREATE_TS, CREATED_BY, VERSION, NAME, LOC_NAME,DESCRIPTION) VALUES (newid(),
CURRENT_TIMESTAMP, USER, 1, 'Master', 'Мастер', '1')^
select create_or_update_sec_permissi('Master', 'workshop$AutoBase:create', 20, 1)^
select create_or_update_sec_permissi('Master', 'workshop$AutoBase:update', 20, 1)^
select create_or_update_sec_permissi('Master', 'workshop$AutoBase:delete', 20, 1)^
select create_or_update_sec_permissi('Master', 'workshop$PurchaseRequest:update', 20, 1)^
select create_or_update_sec_permissi('Master', 'workshop$PurchaseRequest:delete', 20, 1)^
