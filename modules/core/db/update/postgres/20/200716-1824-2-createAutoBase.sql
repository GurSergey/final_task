alter table TASK_AUTO_BASE add constraint FK_TASK_AUTO_BASE_BRAND_ID foreign key (BRAND_ID) references TASK_BRAND(ID);
alter table TASK_AUTO_BASE add constraint FK_TASK_AUTO_BASE_CARD_ID foreign key (CARD_ID) references WF_CARD(ID);
create index IDX_TASK_AUTO_BASE_BRAND on TASK_AUTO_BASE (BRAND_ID);
